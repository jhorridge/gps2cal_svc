function [Cal] = gps2cal(GpsWeek,GpsSecondsOfWeek)
%GPS2CAL Convert GPS format to calendar format.
%
%              Cal -- Structure containing the GPS time converted into
%                     calendar format.
%          GpsWeek -- Current GPS week number.
% GpsSecondsOfWeek -- Number of seconds into the current week.
%
%
% Cal.Year  -- Year e.g. 2016
% Cal.Month -- Month e.g. 2 (Jan=1:Dec=12)
% Cal.Day   -- Day in month e.g. 29 (starting at 1 up to 28,29,30 or 31)
% Cal.Hour  -- Hour into day in 24H format e.g. 17 (00:23)
% Cal.Min   -- Minutes into hour e.g. 39 (00:59)
% Cal.Sec   -- Seconds into minute e.g. 04 (00:59)
% Cal.DoW   -- Day of the week e.g. 3 (Sunday=0:Saturday=6)
%
% GPS epoch is Sunday 6, Jan 1980, 00:00:00
% When GpsWeek=0 and GpsSecondsOfWeek = 0
%
% NOTE: Days are represented as the first day being zero (0), therefore
% when calculating the Day into the month, one (1) must be added at the
% end.
%

% Constants
DaysPerWeek = 7;
HoursPerDay = 24;
SecondsPerMin = 60;
SecondsPerHour = 60*SecondsPerMin;
SecondsPerDay = SecondsPerHour*HoursPerDay;
SecondsPerWeek = SecondsPerDay*DaysPerWeek;

%                    J  F  M  A  M  J  J  A  S  O  N  D
MonthDays = cumsum([31,28,31,30,31,30,31,31,30,31,30,31]); % Non-leap year.


% Check inputs
narginchk(2,2);
if (GpsWeek < 0)
    error('Gps2Cal:invalidValue',...
        'Error - GpsWeek must be a non-negative number.');
end
if (round(GpsWeek) ~= GpsWeek)
    error('Gps2Cal:invalidValue',...
        'Error - GpsWeek must be an integer number.');
end
if (GpsSecondsOfWeek < 0)
    error('Gps2Cal:invalidValue',...
        'Error - GpsSecondsOfWeek must be a non-negative number.');
end
if (GpsSecondsOfWeek >= SecondsPerWeek)
    error('Gps2Cal:invalidValue',...
        'Error - GpsSecondsOfWeek must be a value of 0 to %d',...
        SecondsPerWeek-1);
end


% Construct return value to enforce a particular order for the fields.
Cal = struct('Year',-1,'Month',-1,'Day',-1,'Hour',-1,'Min',-1,'Sec',-1,'DoW',-1);

% Calculate Day of Week (DoW) where 0 is Sunday.
Cal.DoW = floor(GpsSecondsOfWeek./SecondsPerDay);

% Calculate Hour:Minute:Seconds into current day.
SecondsIntoDay = (GpsSecondsOfWeek-Cal.DoW*SecondsPerDay);
Cal.Hour = floor(SecondsIntoDay./SecondsPerHour);
SecondsIntoDay = SecondsIntoDay-Cal.Hour*SecondsPerHour;
Cal.Min = floor(SecondsIntoDay./SecondsPerMin);
Cal.Sec = SecondsIntoDay-Cal.Min*SecondsPerMin;

% Calculate whole number of days and allow for the fact the GPS epoch
% starts on the 6th day of 1980 and not the 1st.
GpsDays = GpsWeek*DaysPerWeek+floor(GpsSecondsOfWeek./SecondsPerDay)+5;

% Calculate the Year
% Note: The year is initialsed with 1979 rather than the epoch year of 1980
% because the while() loop always runs through at least once and thus
% increments the year by one to 1980.
Year=1979;
DaysInYear = 0;
while (GpsDays >= DaysInYear)
    GpsDays = GpsDays-DaysInYear;
    Year = Year+1;
    IsLeap = IsLeapYear(Year);
    if (IsLeap == true)
        DaysInYear = 366;
    else
        DaysInYear = 365;
    end
end

% Calculate the Month.
if (IsLeap == true)
    MonthDays(2:12) = MonthDays(2:12)+ones(1,11);
end
Month=1;
while (GpsDays > MonthDays(Month))
    Month = Month+1;
end

% Calculate Day
if (Month == 1)
    Day = GpsDays;
else
    Day = GpsDays-MonthDays(Month-1);
end

Cal.Year = Year;
Cal.Month = Month;
Cal.Day = Day+1;

function [IsLeap] = IsLeapYear(Year)
if (rem(Year,4))
    IsLeap = false;
elseif (rem(Year,100))
    IsLeap = true;
elseif (rem(Year,400))
    IsLeap = false;
else
    IsLeap = true;
end

