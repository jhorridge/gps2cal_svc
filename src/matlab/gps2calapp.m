function gps2calapp(GpsWeekStr,GpsSecondsOfWeekStr)
%GPS2CALAPP Wrapper function for gps2cal()
%
% This function is a pure wrapper function that is used to convert string
% input arguments into numbers and display the result of the GPS to
% Calendar format conversion.

narginchk(2,2);

GpsWeek = str2double(GpsWeekStr);
GpsSecondsOfWeek = str2double(GpsSecondsOfWeekStr);

Cal = gps2cal(GpsWeek,GpsSecondsOfWeek);

t = datetime(Cal.Year,Cal.Month,Cal.Day,Cal.Hour,Cal.Min,Cal.Sec);
DateString = datestr(t,'ddd dd mmm yyyy HH:MM:SS');

fprintf('%s UTC\n',DateString);

end
