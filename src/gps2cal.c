#include <stdlib.h>
#include <stdio.h>

#include <matrix.h>

#include "libgps2cal.h"

typedef struct {
  int year;
  int month;
  int day;
  int hour;
  int minute;
  int second;
  int week_day;
} cal_t;

static char *dow2string[] = {
  "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat",
};

static char *month2string[] = {
    "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",
};

void usage (FILE *stream, char const *name, int rtc)
{
  char const help_string[] = {
    "Usage: %s WEEK SECS\n"
    "\n"
    "Convert GSP WEEK number and SECS into week into\n"
    "calendar format of Year, Month, Day, Hour, Minutes,\n"
    "Seconds and Day-of-Week.\n"
  };

  fprintf(stream, help_string, name);
  exit(rtc);
}

int run_main(int argc, const char **argv)
{
  if (argc < 3) {
    fprintf(stderr,
      "Invalid number of arguments, got %d, expected %d\n",
      argc, 2);

    usage(stderr, argv[0], EXIT_FAILURE);
  }

  int Week = (int)strtoul(argv[1], NULL, 10);
  int SecondsOfWeek = (int)strtoul(argv[2], NULL, 10);

  /* Call the library intialization routine and make sure that the
   * library was initialized properly
   */
  if (!libgps2calInitialize()) {
      fprintf(stderr, "could not initialize the gps2cal library properly\n");
      return (EXIT_FAILURE);
  }

  mxArray *GpsWeek = mxCreateDoubleScalar(Week);
  mxArray *GpsSecondsOfWeek = mxCreateDoubleScalar(SecondsOfWeek);
  mxArray *Cal = NULL;

  bool status = mlfGps2cal(1, &Cal, GpsWeek, GpsSecondsOfWeek);

  if (status != true) {
    fprintf(stderr, "%s: %d: mlfGps2cal() failed\n", __FUNCTION__, __LINE__);
    exit(EXIT_FAILURE);
  }

  cal_t cal;

  cal.year = (int)mxGetDoubles(mxGetField(Cal, 0, "Year"))[0];
  cal.month = (int)mxGetDoubles(mxGetField(Cal, 0, "Month"))[0];
  cal.day = (int)mxGetDoubles(mxGetField(Cal, 0, "Day"))[0];
  cal.hour = (int)mxGetDoubles(mxGetField(Cal, 0, "Hour"))[0];
  cal.minute = (int)mxGetDoubles(mxGetField(Cal, 0, "Min"))[0];
  cal.second = (int)mxGetDoubles(mxGetField(Cal, 0, "Sec"))[0];
  cal.week_day = (int)mxGetDoubles(mxGetField(Cal, 0, "DoW"))[0];

  mxDestroyArray(Cal);
  Cal = NULL;
  mxDestroyArray(GpsWeek);
  GpsWeek = NULL;
  mxDestroyArray(GpsSecondsOfWeek);
  GpsSecondsOfWeek = NULL;

  /* Call the library termination routine */
  libgps2calTerminate();

  /* Note that you should call mclTerminateApplication at the end of
   * your application.
   */
  mclTerminateApplication();

  //fprintf(stdout, "GPS Time: %d week %d seconds-of-week\n", Week, SecondsOfWeek);
  fprintf(stdout,"%s %02d %s %04d\n",
          dow2string[cal.week_day % 7],
          cal.day,
          month2string[cal.month-1], cal.year);
  fprintf(stdout, "Cal Time: %04d-%02d-%02d %02d:%02d:%02d %s\n",
          cal.year, cal.month, cal.day,
          cal.hour, cal.minute, cal.second,
          dow2string[cal.week_day % 7]);

  return (0);
}

int main (int argc, const char **argv)
{
  /* Call the mclInitializeApplication routine. Make sure that the application
   * was initialized properly by checking the return status. This initialization
   * has to be done before calling any MATLAB APIs or MATLAB Compiler SDK
   * generated shared library functions.  */
  if (!mclInitializeApplication(NULL, 0))
  {
      fprintf(stderr, "Could not initialize the application.\n");
      return -1;
  }
  return (mclRunMain((mclMainFcnType)run_main, argc, argv));
}
